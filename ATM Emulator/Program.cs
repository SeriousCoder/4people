﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;

namespace ATM_Emulator
{
    class Bills
    {
        public int Par;
        public int Count;

        public Bills(int par, int count)
        {
            Par = par;
            Count = count;
        }

        public int TotalSum()
        {
            return Par * Count;
        }
    }

    class ATM
    {
        List<Bills> BillsHub = new List<Bills>();
        private int minPar = 0;

        public ATM()
        {
            BillsHub = new List<Bills>();
        }

        public void LoadBills(string[] bills)
        {
            foreach (var bill in bills)
            {
                var foo = bill.Split(":");

                BillsHub.Add(new Bills(int.Parse(foo[0]), int.Parse(foo[1])));
            }

            BillsHub.Sort(delegate (Bills a, Bills b) { return a.Par.CompareTo(b.Par); });
        }

        public string TryEraseMoney(int moneySum)
        {
            var dp = new List<Bills>[moneySum + 1];
            
            dp[0] = new List<Bills>();
            for (int j = 0; j < BillsHub.Count; j++)
            {
                if (!dp[0].Any(x => x.Par == BillsHub[j].Par))
                    dp[0].Add(new Bills(BillsHub[j].Par, 0));
            }

            for (int i = 1; i <= moneySum; i++)
            {
                dp[i] = new List<Bills>();

                for (int j = 0; j < BillsHub.Count; j++)
                {
                    if (!dp[i].Any(x => x.Par == BillsHub[j].Par))
                        dp[i].Add(new Bills(BillsHub[j].Par, 0));
                }

                    var totalBills = int.MaxValue;
                for (int j = 0; j < BillsHub.Count; j++)
                {
                    //if (!dp[i].Any(x => x.Par == BillsHub[j].Par))
                     //   dp[i].Add(new Bills(BillsHub[j].Par, 0));

                    if (i - BillsHub[j].Par < 0) continue;

                    var sumOfCountBills = dp[i - BillsHub[j].Par].Sum(x => x.Count);

                    if (i >= BillsHub[j].Par && sumOfCountBills + 1 < totalBills && dp[i - BillsHub[j].Par].First(x => x.Par == BillsHub[j].Par).Count < BillsHub[j].Count)
                    {
                        for (int k = 0; k < BillsHub.Count; k++)
                        {
                            dp[i].First(x => x.Par == BillsHub[k].Par).Par = dp[i - BillsHub[j].Par].First(x => x.Par == BillsHub[k].Par).Par;
                            dp[i].First(x => x.Par == BillsHub[k].Par).Count = dp[i - BillsHub[j].Par].First(x => x.Par == BillsHub[k].Par).Count;
                        }
                        

                        dp[i].First(x => x.Par == BillsHub[j].Par).Count = dp[i - BillsHub[j].Par].First(x => x.Par == BillsHub[j].Par).Count + 1;
                        totalBills = dp[i].Sum(x => x.Count);
                    }
                }
            }

            if (dp[moneySum].Sum(x => x.Count) == 0 || dp[moneySum].Sum(x => x.TotalSum()) != moneySum) return "Банкомат не может выдать данную сумму";

            var outputInfo = "";

            foreach (var billse in dp[moneySum])
            {
                if (billse.Count == 0) continue;
                outputInfo += $"{billse.Par}:{billse.Count}\n";
            }

            return outputInfo;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            var money = new List<Bills>();
            var atm = new ATM();

            Console.WriteLine("Чтение файла с кол-вом купюр");

            string[] billsFileInfo = System.IO.File.ReadAllLines(@"C:\Temp\ATM.txt");

            atm.LoadBills(billsFileInfo);

            Console.WriteLine("Все купюры прочитаны");
            Console.WriteLine("Введите нужную сумму для выдачи. Для выхода напишите 'exit'");

            var userInput = Console.ReadLine()?.ToLower();

            while (userInput != "exit")
            {
                int userInputSum;

                if (int.TryParse(userInput, out userInputSum))
                    Console.WriteLine(atm.TryEraseMoney(userInputSum));

                userInput = Console.ReadLine()?.ToLower();
            }
        }
    }
}
